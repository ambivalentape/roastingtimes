module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput)


type Animal
    = Pork
    | Chicken
    | Turkey
    | Beef
    | Lamb
    | Venison


type WeightUnit
    = Grams
    | Kilograms
    | Ounce
    | Pound


type Doneness
    = Rare
    | MediumRare
    | Medium
    | Well


type alias Weight =
    { amount : Float, weightUnit : WeightUnit }


type TemperatureScale
    = Celsius
    | Fahrenheit


normaliseWeight : Weight -> Weight
normaliseWeight weight =
    case weight.weightUnit of
        Grams ->
            weight

        Kilograms ->
            Weight (weight.amount * 1000) Grams

        Ounce ->
            Weight (weight.amount * 28.35) Grams

        Pound ->
            Weight ((normaliseWeight (Weight (weight.amount * 16) Ounce)).amount) Grams



--'Sizzle' time on high heat (~220c).
-- This time isn't linear with size of the joint being roasted,
-- so we cap it at the extremes


calculateSizzle : Weight -> Animal -> Int
calculateSizzle weight animal =
    case animal of
        Chicken ->
            20

        _ ->
            case weight.weightUnit of
                Grams ->
                    if round (weight.amount / 100) > 40 then
                        40
                    else if round (weight.amount / 100) < 20 then
                        20
                    else
                        round (weight.amount / 100)

                _ ->
                    calculateSizzle (normaliseWeight (weight)) animal


calculateMain : Weight -> Animal -> Doneness -> Int
calculateMain weight animal doneness =
    case weight.weightUnit of
        Grams ->
            round ((weight.amount / 500) * toFloat (calculateMultiplier weight animal doneness))

        _ ->
            calculateMain (normaliseWeight (weight)) animal doneness


isLarge : Weight -> Bool
isLarge weight =
    case weight.weightUnit of
        Grams ->
            weight.amount >= 5000

        -- this is coarse, but difficult to account for all the factors in this
        _ ->
            isLarge (normaliseWeight (weight))


calculateMultiplier : Weight -> Animal -> Doneness -> Int
calculateMultiplier weight animal doneness =
    case weight.weightUnit of
        Grams ->
            case animal of
                Chicken ->
                    15

                Turkey ->
                    20

                Pork ->
                    25

                -- never undercook pork : awful idea
                _ ->
                    case doneness of
                        Rare ->
                            if isLarge (weight) then
                                9
                            else
                                10

                        MediumRare ->
                            if isLarge (weight) then
                                10
                            else
                                12

                        Medium ->
                            if isLarge (weight) then
                                12
                            else
                                15

                        Well ->
                            if isLarge (weight) then
                                18
                            else
                                20

        _ ->
            calculateMultiplier (normaliseWeight (weight)) animal doneness


calculateTotalRoastingTime : Weight -> Animal -> Doneness -> ( Int, Int )
calculateTotalRoastingTime weight animal doneness =
    ( calculateSizzle weight animal, calculateMain weight animal doneness )


type alias Model =
    { animal : Animal
    , weight : Float
    , weightUnit : WeightUnit
    , doneness : Doneness
    , sizzleTime : Int
    , cookingTime : Int
    }


model : Model
model =
    Model Beef 2500 Grams Rare (calculateSizzle (Weight 2500 Grams) Beef) (calculateMain (Weight 2500 Grams) Beef Rare)


type Msg
    = SetAnimalName String
    | SetAnimalWeight String
    | SetAnimalWeightUnit String
    | SetAnimalDoneness String


parseWeightUnit : String -> WeightUnit
parseWeightUnit s =
    case s of
        "Grams" ->
            Grams

        "Ounce" ->
            Ounce

        "Pound" ->
            Pound

        "Kilograms" ->
            Kilograms

        _ ->
            Grams


parseAnimal : String -> Animal
parseAnimal s =
    case s of
        "Beef" ->
            Beef

        "Lamb" ->
            Lamb

        "Chicken" ->
            Chicken

        "Venison" ->
            Venison

        "Turkey" ->
            Turkey

        "Pork" ->
            Pork

        _ ->
            Chicken


parseDoneness : String -> Doneness
parseDoneness s =
    case s of
        "Rare" ->
            Rare

        "MediumRare" ->
            MediumRare

        "Medium" ->
            Medium

        "Well" ->
            Well

        _ ->
            Medium


update : Msg -> Model -> Model
update msg model =
    case msg of
        SetAnimalName sname ->
            let
                name =
                    parseAnimal sname
            in
                { model | animal = name, sizzleTime = calculateSizzle (Weight model.weight model.weightUnit) name }

        SetAnimalWeight weight ->
            let
                aweight =
                    Weight (Result.withDefault 0 (weight |> String.toFloat)) model.weightUnit
            in
                { model
                    | weight = aweight.amount
                    , sizzleTime = calculateSizzle aweight model.animal
                    , cookingTime = calculateMain aweight model.animal model.doneness
                }

        SetAnimalWeightUnit weightUnit ->
            let
                parsedUnit =
                    parseWeightUnit weightUnit
            in
                { model
                    | weightUnit = parsedUnit
                    , sizzleTime = calculateSizzle (Weight model.weight parsedUnit) model.animal
                    , cookingTime = calculateMain (Weight model.weight parsedUnit) model.animal model.doneness
                }

        SetAnimalDoneness doneness ->
            let
                parsedDoneness =
                    parseDoneness doneness
            in
                { model
                    | doneness = parsedDoneness
                    , sizzleTime = calculateSizzle (Weight model.weight model.weightUnit) model.animal
                    , cookingTime = calculateMain (Weight model.weight model.weightUnit) model.animal parsedDoneness
                }


temperatures : Animal -> ( Int, Int )
temperatures animal =
    case animal of
        Chicken ->
            ( 180, 210 )

        Turkey ->
            ( 180, 210 )

        _ ->
            ( 160, 230 )


convertToFahrenheit : Int -> Int
convertToFahrenheit temp =
    round (toFloat (temp) * (9.0 / 5.0) + 32.0)


animalOption animal =
    option [ value (toString animal) ] [ text (toString animal) ]


donenessOption dn =
    option [ value (toString dn) ] [ text (toString dn) ]


weightUnitOption dn =
    option [ value (toString dn) ] [ text (toString dn) ]


view : Model -> Html Msg
view model =
    let
        ( lowTemp, highTemp ) =
            temperatures model.animal
    in
        div [ class "container" ]
            [ div [ class "row" ]
                [ div [ class "col-md-5" ]
                    [ h3 []
                        [ span [ class "label label-primary" ] [ text "meat" ] ]
                    , select
                        [ onInput SetAnimalName ]
                        ([ Beef, Chicken, Lamb, Venison, Pork, Turkey ] |> List.map animalOption)
                    ]
                ]
            , p [] []
            , div [ class "row" ]
                [ div [ class "col-md-5" ]
                    [ h3 []
                        [ span [ class "label label-primary" ] [ text "weight" ] ]
                    , input [ type_ "text", value (toString model.weight), onInput SetAnimalWeight ] []
                    , p [] []
                    , select
                        [ onInput SetAnimalWeightUnit ]
                        ([ Grams, Kilograms, Pound, Ounce ] |> List.map weightUnitOption)
                    ]
                ]
            , p [] []
            , div [ class "row" ]
                [ div [ class "col-md-5" ]
                    [ h3 []
                        [ span [ class "label label-primary" ] [ text "done-ness" ] ]
                    , select [ onInput SetAnimalDoneness ] ([ Rare, MediumRare, Medium, Well ] |> List.map donenessOption)
                    ]
                ]
            , p [] []
            , div [ class "row" ]
                [ div [ class "col-md-5" ]
                    [ div [ class "panel panel-default" ]
                        [ div [ class "panel-heading" ] [ div [] [ text "Instructions" ] ]
                        , div [ class "panel-body" ]
                            [ div []
                                [ p [] [ strong [] [ text "sizzle for " ], span [] [ text (" " ++ (toString model.sizzleTime) ++ " minutes at " ++ (toString highTemp) ++ "C/" ++ (toString (convertToFahrenheit highTemp)) ++ "F") ] ]
                                , p [] [ strong [] [ text "then" ] ]
                                , p [] [ strong [] [ text "turn down the heat " ], span [] [ text ("to " ++ (toString lowTemp) ++ "C/" ++ (toString (convertToFahrenheit lowTemp)) ++ "F and roast for " ++ (toString model.cookingTime) ++ " minutes") ] ]
                                , p [] [ strong [] [ text "then" ], span [] [ text " rest for 15-20 minutes" ] ]
                                ]
                            ]
                        ]
                    ]
                ]
            , div [ class "row" ]
                [ div [ class "col-md-5" ]
                    [ h6 []
                        [ p [] [ text "disclaimer: this site is for guidance only - you should invest in a decent meat thermometer and not eat anything you're not sure about." ]
                        , p [] [ text "A 2018 production by ambivalenta.pe" ]
                        ]
                    ]
                ]
            ]


main =
    Html.beginnerProgram { model = model, view = view, update = update }
