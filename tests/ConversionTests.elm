module ConversionTests exposing (..)

import Expect exposing (Expectation)
import Test exposing (..)
import Main exposing (..)


suite : Test
suite =
    describe "RoastingTimes test suit"
        [ describe "Utility tests"
            [ describe "Weight Unit Conversion tests"
                [ test "Grams" <|
                    \() -> Expect.equal (Weight 1 Grams) (Weight 1 Grams)
                , test "Kilos" <|
                    \() -> Expect.equal (Weight 1000 Grams) (normaliseWeight (Weight 1 Kilograms))
                , test "Ounces" <|
                    \() -> Expect.equal (Weight 28.35 Grams) (normaliseWeight (Weight 1 Ounce))
                , test "Pounds" <|
                    \() -> Expect.equal (Weight 453.6 Grams) (normaliseWeight (Weight 1 Pound))
                ]
            , describe "Largeness tests"
                [ test "Big" <|
                    \() -> Expect.equal (isLarge (Weight 5000 Grams)) True
                , test "Regular" <|
                    \() -> Expect.equal (isLarge (Weight 4999 Grams)) False
                ]
            ]
        ]
