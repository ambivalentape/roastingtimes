module CookingTests exposing (..)

import Expect exposing (Expectation)
import Test exposing (..)
import Main exposing (..)


suite : Test
suite =
    describe "RoastingTimes Cooking test suite"
        [ describe "Sizzle tests"
            [ test "Large" <|
                \() -> Expect.equal (calculateSizzle (Weight 4500 Grams) Beef) 40
            , test "Small" <|
                \() -> Expect.equal (calculateSizzle (Weight 1900 Grams) Beef) 20
            , test "Normal" <|
                \() -> Expect.equal (calculateSizzle (Weight 2900 Grams) Beef) 29
            , test "Normal Rounded" <|
                \() -> Expect.equal (calculateSizzle (Weight 2970 Grams) Beef) 30
            , test "Normal Ounces" <|
                -- check the conversion works
                \() -> Expect.equal (calculateSizzle (Weight 5.14 Pound) Beef) 23
            , test "Chicken" <|
                -- this should be 20 no matter what
                \() -> Expect.equal (calculateSizzle (Weight 2900 Grams) Chicken) 20
            ]
        , describe "Main tests"
            -- for chicken, it's always 15m/500g (so doneness is irrelevant to the calc)
            [ test "Chicken small rare " <|
                \() ->
                    Expect.equal (calculateMain (Weight 1500 Grams) Chicken Rare) 45
            , test "Chicken small med rare" <|
                \() ->
                    Expect.equal (calculateMain (Weight 1500 Grams) Chicken MediumRare) 45
            , test "Chicken small med" <|
                \() ->
                    Expect.equal (calculateMain (Weight 1500 Grams) Chicken Medium) 45
            , test "Chicken small well" <|
                \() ->
                    Expect.equal (calculateMain (Weight 1500 Grams) Chicken Well) 45
            , test "Chicken large well" <|
                \() ->
                    Expect.equal (calculateMain (Weight 5000 Grams) Chicken Well) 150

            -- simillarly, pork is always 25m/500g
            , test "Pork large rare" <|
                \() ->
                    Expect.equal (calculateMain (Weight 5000 Grams) Pork Rare) 250
            , test "Pork large med rare" <|
                \() ->
                    Expect.equal (calculateMain (Weight 5000 Grams) Pork MediumRare) 250
            , test "Pork large med" <|
                \() ->
                    Expect.equal (calculateMain (Weight 5000 Grams) Pork Medium) 250
            , test "Pork large well" <|
                \() ->
                    Expect.equal (calculateMain (Weight 5000 Grams) Pork Well) 250
            , test "Beef large rare" <|
                \() ->
                    Expect.equal (calculateMain (Weight 5000 Grams) Beef Rare) 90
            , test "Beef large med rare" <|
                \() ->
                    Expect.equal (calculateMain (Weight 5000 Grams) Beef MediumRare) 100
            , test "Beef large med" <|
                \() ->
                    Expect.equal (calculateMain (Weight 5000 Grams) Beef Medium) 120
            , test "Beef large well" <|
                \() ->
                    Expect.equal (calculateMain (Weight 5000 Grams) Beef Well) 180
            , test "Beef reg rare" <|
                \() ->
                    Expect.equal (calculateMain (Weight 3000 Grams) Beef Rare) 60
            , test "Beef reg med rare" <|
                \() ->
                    Expect.equal (calculateMain (Weight 3000 Grams) Beef MediumRare) 72
            , test "Beef reg med" <|
                \() ->
                    Expect.equal (calculateMain (Weight 3000 Grams) Beef Medium) 90
            , test "Beef reg well" <|
                \() ->
                    Expect.equal (calculateMain (Weight 3000 Grams) Beef Well) 120
            ]
        ]
